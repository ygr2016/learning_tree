package com.tools;

/**
 * Created by cl_kd-user47823 on 2021/6/30.
 */
public class LeetCodeTools {
    /**
     * 获取随机字符串
     * @param num 需要的字符串长度
     * param charType 字符串类型 1小写 2大写 默认大小写随机
     * @return
     */
    public static String getRandomWord(int num,int charType){
        String str="";
        for(int i=0;i<num;i++){
            if(charType==1){
                str+=(char)(97+new Double(Math.random()*26).intValue());
            }else if(charType==2){
                str+=(char)(65+new Double(Math.random()*26).intValue());
            }else{
                if(new Double(Math.random()*100).intValue()%2==0){
                    str+=(char)(97+new Double(Math.random()*26).intValue());
                }else{
                    str+=(char)(65+new Double(Math.random()*26).intValue());
                }
            }
        }
        return str;
    }
    public static int[] getIntArr(int arrCount,int numSize){
        int[] arr=new int[arrCount];
        for(int i=0;i<arr.length;i++){
            arr[i]=(int)Math.round(Math.random()*numSize);
        }
        return arr;
    }
    public static void printIntArr(int[] arr){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+"   ");
        }
        System.out.println();
    }

}