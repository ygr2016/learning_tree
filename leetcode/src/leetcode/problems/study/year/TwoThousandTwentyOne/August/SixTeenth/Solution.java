package leetcode.problems.study.year.TwoThousandTwentyOne.August.SixTeenth;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cl_kd-user47823 on 2021/8/16.
 */
public class Solution {
    public static boolean canPermutePalindrome(String str) {
        boolean flag=true;

        HashMap<String,Integer> map=new HashMap<String,Integer>();
        for(int i=0;i<str.length();i++){
            if(map.get(str.charAt(i)+"")!=null){
                map.put(str.charAt(i)+"",(Integer)map.get(str.charAt(i)+"")+1);
            }else{
                map.put(str.charAt(i)+"",1);
            }
        }
        int num=0;
        for(String n:map.keySet()){
            if(map.get(n)%2!=0){
                num++;
            }
            if(num>=2){
                flag=false;
                break;
            }

        }

        return flag;
    }
    public static boolean canPermutePalindrome2(String str) {
        Set<Character> ch=new HashSet<Character>();
        for(int i=0;i<str.length();i++){
            if(!ch.add(str.charAt(i))){
                ch.remove(str.charAt(i));
            }
        }
        return ch.size()<=1;
    }
    public static void main(String[] args) {
        long time=new Date().getTime();
        System.out.println( oneEditAway("pales","pal"));
        System.out.println("endtime"+(new Date().getTime()-time));
    }
    public static boolean oneEditAway(String first, String second) {
        int num=first.length()-second.length();
        if(num>=-1&&num<=1){

        }
        return false;
    }
}
